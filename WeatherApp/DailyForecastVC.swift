//
//  DailyForecastVC.swift
//  WeatherApp
//
//  Created by Silvia Florido on 16/02/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import UIKit

class DailyForecastVC: UIViewController, UITableViewDataSource, UICollectionViewDataSource {

    @IBOutlet weak var currentTempLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    
    @IBOutlet weak var weekdayLabel: UILabel!
    @IBOutlet weak var todayMaxTempLabel: UILabel!
    @IBOutlet weak var todayMinTempLabel: UILabel!
    @IBOutlet weak var hourlyCollection: UICollectionView!
    
    
    let dailyCellId = "dailyWeatherCell"
    let hourlyCellId = "hourlyCell"
    
    var location: UserLocation?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = location?.name
        location?.fetchWeather(fetchType: .hourly, completionHandler: {
            DispatchQueue.main.async {
                self.hourlyCollection.reloadData()
            }
        })
        setupViews()
        
        
    }

    
    func setupViews() {
        if let current =  location?.currentWeather {
            currentTempLabel.text = String(format: "%.0f", current.currentTemperature)
            summaryLabel.text = location?.currentWeather?.shortSummary
        }
        if let today = location?.todayForecast {
            weekdayLabel.text = today.utcDate.weekdayName
            if let max = today.temperatureHigh, let min = today.temperatureLow {
                todayMaxTempLabel.text = String(format: "%.0f°", max)
                todayMinTempLabel.text = String(format: "%.0f°", min)
            }
        }
        
    }

    
    
    
    
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return location?.weekForecast?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: dailyCellId, for: indexPath) as! DailyForecastCell
        
        if let week = location?.weekForecast {
            let daily = week[indexPath.row]

            cell.weekdayLabel.text = daily.utcDate.longWeekdayName
            if let max = daily.temperatureHigh, let min = daily.temperatureLow {
                cell.maxTempLabel.text = String(format: "%.0f", max)
                cell.minTempLabel.text = String(format: "%.0f", min)
            }
            if let icon = daily.icon {
                cell.iconImage.image = UIImage(named: icon)
            }        
        }
        
        return cell
    }
    
    
    
    
    
    // MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return location?.todayForecast?.hourly?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: hourlyCellId, for: indexPath) as! HourlyCell
        
        if let hourlyArray = location?.todayForecast?.hourly  {
            let hourly = hourlyArray[indexPath.row]
            let f = DateFormatter()
            f.dateFormat = "h a"
            cell.hourLabel.text = f.string(from: hourly.utcTime)
            
            if let icon = hourly.icon {
                cell.iconImage.image = UIImage(named: icon)
            }
            
            cell.temperatureLabel.text = String(format: "%.0f°", hourly.temperature!)
        }
        
        
        return cell
    }

}




















