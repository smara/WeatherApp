//
//  LocationCell.swift
//  WeatherApp
//
//  Created by Silvia Florido on 26/01/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import UIKit

class LocationCell: UITableViewCell {
    
    @IBOutlet weak var placemarkLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var currentTemperatureLabel: UILabel!
    @IBOutlet weak var degreeLabel: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        placemarkLabel.text = ""
        summaryLabel.text = ""
        currentTemperatureLabel.text = ""
//        degreeLabel.text = ""
        iconImage.image = nil
    }
    
    
 
}
