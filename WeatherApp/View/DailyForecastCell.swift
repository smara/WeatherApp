//
//  DailyForecastCell.swift
//  WeatherApp
//
//  Created by Silvia Florido on 16/02/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import UIKit

class DailyForecastCell: UITableViewCell {

    
    @IBOutlet weak var weekdayLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    
    
    
}
