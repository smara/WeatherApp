//
//  LocationsTableHeaderView.swift
//  WeatherApp
//
//  Created by Silvia Florido on 29/01/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import UIKit

class LocationsTableHeaderView: UIView {

    @IBOutlet weak var placemarkLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var currentTemperatureLabel: UILabel!
    @IBOutlet weak var degreeLabel: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var hiLabel: UILabel!
    @IBOutlet weak var loLabel: UILabel!
    @IBOutlet weak var noLocationsLabel: UILabel!
    
    var isLocationSet: Bool = true {
        didSet {
            
            let x =  self.subviews.filter{ $0 !== noLocationsLabel}
            x.forEach { (v) in
                v.isHidden = !isLocationSet
            }
//            self.subviews.filter{ $0 !== noLocationsLabel}
//                .forEach{ $0.isHidden = isLocationSet}
            noLocationsLabel.isHidden = isLocationSet
        }
    }
    
    func cleanUI() {
        placemarkLabel.text = ""
        summaryLabel.text = ""
        currentTemperatureLabel.text = ""
        degreeLabel.text = ""
        iconImage.image = nil
        hiLabel.text = ""
        loLabel.text = ""
    }
    
    
    
    
}

















