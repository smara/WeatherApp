//
//  HourlyCell.swift
//  WeatherApp
//
//  Created by Silvia Florido on 18/02/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import UIKit

class HourlyCell: UICollectionViewCell {
    
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    
}
