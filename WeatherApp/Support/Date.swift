//
//  Date.swift
//  Workout App
//
//  Created by Silvia Florido on 12/14/16.
//  Copyright © 2016 Silvia Florido. All rights reserved.
//
// Adds functionality to get components from Date()
//
//  Date().shortWeekdays will return Wed, Sat etc
//

//   To work with Strings (weekday name) use formatters. To work with numbers use Components 
/*
 Another option:
 
 let gregorianCalendar = Calendar(identifier: .gregorian)
 let weekday = gregorianCalendar.component(.weekday, from: Date())
 let weekdayName = gregorianCalendar.shortWeekdaySymbols[weekday - 1]
 
 */
 
 


import Foundation

enum WeekdayEnum: Int {
    
    case Sun = 0
    case Mon
    case Tue
    case Wed
    case Thu
    case Fri
    case Sat
    
    
    func simpleDescription() -> String {
        switch self {
        case .Sun:
            return "Sun"
        case .Mon:
            return "Mon"
        case .Tue:
            return "Tue"
        case .Wed:
            return "Wed"
        case .Thu:
            return "Thu"
        case .Fri:
            return "Fri"
        case .Sat:
            return "Sat"
        }
    }
    
    static func stringToWeekday(value: String) -> WeekdayEnum? {
        switch value {
        case "Sun", "Sunday":
            return WeekdayEnum.Sun
        case "Mon", "Monday":
            return WeekdayEnum.Mon
        case "Tue", "Tuesday":
            return WeekdayEnum.Tue
        case "Wed", "Wednesday":
            return WeekdayEnum.Wed
        case "Thu", "Thursday":
            return WeekdayEnum.Thu
        case "Fri", "Friday":
            return WeekdayEnum.Fri
        case "Sat", "Saturday":
            return WeekdayEnum.Sat
        default:
            return nil
        }
    }
    
    static func intWeekdayToString(value: Int) -> String? {
        switch value {
        case 0:
            return "Sun"
        case 1:
            return "Mon"
        case 2:
            return "Tue"
        case 3:
            return "Wed"
        case 4:
            return "Thu"
        case 5:
            return "Fri"
        case 6:
            return "Sat"
        default:
            return nil
        }
        
    }
    
    
}

extension Date {
    struct Formatters {
        static let custom: DateFormatter = {
            let formatter = DateFormatter()
            return formatter
        }()
        static let dateLocal:DateFormatter = {
            let formatter = DateFormatter()
            formatter.timeZone = Calendar.current.timeZone
            formatter.dateStyle = .medium
            formatter.timeStyle = .medium

            return formatter
        }()
        static let date:DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            return formatter
        }()
        static let todayMDY:DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "MM-dd-yyyy"
            return formatter
        }()
        static let todayDMY:DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            return formatter
        }()
        static let time:DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            return formatter
        }()
        static let weekday: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "cccc"
            return formatter
        }()
        static let shortWeekdayName: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "EEE"
            return formatter
        }()
        static let longWeekdayName: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "EEEE"
            return formatter
        }()

        static let month: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "LLLL"
            return formatter
        }()
    }
    
    // from DateComponents you can get local hour, minute, month, year etc
    var dateLocalComponents:DateComponents {
        return Calendar.current.dateComponents(in: .current, from: self)
    }
    /*
    // returns UTC (Date) 
    var dateLocal: Date{
        let currentDateString = Formatters.dateLocal.string(from: self)
        let currentDate = Calendar.current.date(from: self.dateLocalComponents)//Formatters.dateLocal.date(from: currentDateString)
        return currentDate!
    }*/
    var todayMDY: String {
        return Formatters.todayMDY.string(from: self)
    }
    var todayDMY: String {
        return Formatters.todayDMY.string(from: self)
    }
    var dateString: String {
        return Formatters.date.string(from: self)
    }
    var timeString: String {
        return Formatters.time.string(from: self)
    }
    var weekdayName: String {
        return Formatters.weekday.string(from: self)
    }
    var shortWeekdayName: String {
        return Formatters.shortWeekdayName.string(from: self)
    }
    var longWeekdayName: String {
        return Formatters.longWeekdayName.string(from: self)
    }
    var monthName: String {
        return Formatters.month.string(from: self)
    }
    func formatted(with dateFormat: String) -> String {
        Formatters.custom.dateFormat = dateFormat
        return Formatters.custom.string(from: self)
    }
    
    
    
    
}
