//
//  CurrentWeatherVC.swift
//  WeatherApp
//
//  Created by Silvia Florido on 08/02/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//
//
//  Location obtained using Apple's Standard Location Services (GPS) for accuracy (first run or when no location was found). Places details and Autocomplete provided by Google API.

import UIKit
import CoreLocation
import Darwin
import GooglePlaces

class CurrentWeatherVC: UIViewController, CLLocationManagerDelegate, UserLocationDelegate {
    
    @IBOutlet weak var shortSummary: UILabel!
    @IBOutlet weak var degreeLabel: UILabel!
    @IBOutlet weak var currentTemperature: UILabel!
    @IBOutlet weak var iconImg: UIImageView!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var feelsLikeLabel: UILabel!
    @IBOutlet weak var hiLoLabel: UILabel!
    
    
    @IBOutlet weak var precipitationLabel: UILabel!
    @IBOutlet weak var sunriseLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var sunsetLabel: UILabel!
    
    
    
    
    // MARK: - Model
    var selectedLocation: UserLocation? {
        return UserLocation.selected
    }
    
    lazy var allLocations: [UserLocation] = UserLocation.allLocations
    
    var currentDeviceLocation: UserLocation?
    
    lazy var manager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        return manager
    }()
    
    
    // MARK: - VC Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        if selectedLocation != nil {
            fetchWeather(for: selectedLocation!)
        } else {
            manager.requestLocation()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let selected = selectedLocation,  selected.todayForecast == nil {
            fetchWeather(for: selected)
        } else {
            updateUI()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showLocationsList" {
            // nothing to pass by now
        } else if segue.identifier == "showDailyForecast" {
            if let destination = segue.destination as? DailyForecastVC {
                destination.location = selectedLocation
            }
        }
    }
    
    
    
    private func updateUI() {
        guard let location = UserLocation.selected else {
            // show warning
            // clean UI ?
            return
        }
        
        locationButton.setTitle(location.name ?? location.coordinate, for: .normal)
        
        // current (moment) info
        if let currentWeather = location.currentWeather {
            feelsLikeLabel.text = ""
            shortSummary.text = currentWeather.shortSummary
            currentTemperature.text = String(format: "%.0f",currentWeather.currentTemperature)
            
            if let feelsLike = currentWeather.feelsLike {
                feelsLikeLabel.text = String(format: "Sensação: %.0f °C", feelsLike)
            }
            if let img = currentWeather.icon {
                iconImg.image = UIImage(named: img)
            }
            if let speed = currentWeather.windSpeed, let direction =  currentWeather.windDirection {
                windLabel.text = String(format: "%.0f km/h %@", speed, direction)
            }
        } else {
            print("\n\n Can't update current weather. \n\n")
        }
        
        // daily info
        if let today = location.todayForecast {
            hiLoLabel.text = ""
            precipitationLabel.text = ""
            windLabel.text = ""
            
            if let high = today.temperatureHigh, let low = today.temperatureLow {
                hiLoLabel.text = String(format: "Max: %.0f°   Min: %.0f°", high,low)
            }
            if let precipitation = today.precipProbability {
                precipitationLabel.text = (String(format: "%.0f",precipitation) + " %")
            }
            sunriseLabel.text = today.sunriseTime ?? ""
            sunsetLabel.text = today.sunsetTime ?? ""
        } else {
            print("\n\n Can't update forecast. \n\n")
        }
        
    }
    
    
    
    
    
    // MARK: - Helpers
    private func fetchWeather(for userLocation: UserLocation) {
        userLocation.fetchWeather(fetchType: .all) { [weak self] in
            DispatchQueue.main.async {
                self?.updateUI()
            }
        }
    }

    
    
    
    
    
    
    
    // MARK: - CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let newCLLocation = locations.last {

            // if this method is called more than once discard old and keep only last
            if currentDeviceLocation != nil {
                if let index = allLocations.index(of: currentDeviceLocation!) {
                    allLocations.remove(at: index)
                }
            }
            
            let newUserLocation = UserLocation(with: newCLLocation)
            newUserLocation.delegate = self
            newUserLocation.becomeSelectedLocation()
            newUserLocation.reverseGeoCode()
            allLocations.append(newUserLocation)
            fetchWeather(for: newUserLocation)
            currentDeviceLocation = newUserLocation
            
            print("Location created: ", newUserLocation.cLLocation)
        }
    }


    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
        // ask for more info
    }

    
    
    
    
    
    // MARK: - UserLocationDelegate
    func didSetSelectedLocationName(name: String) {
        locationButton.setTitle(name, for: .normal)
    }
    
   
    
    
}



