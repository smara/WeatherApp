//
//  UserLocation.swift
//  WeatherApp
//
//  Created by Silvia Florido on 07/02/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import Foundation
import CoreLocation
import os.log

enum FetchType: String {
    case daily = "?units=si&exclude=minutely,hourly,currently"
    case current = "?units=si&exclude=minutely,hourly,daily"
    case hourly = "?units=si&exclude=currently,minutely,daily"
    case all = "?units=si&exclude=minutely,hourly"
}




protocol UserLocationDelegate: class {
    func didSetSelectedLocationName(name: String)
}




class UserLocation: NSObject, NSCoding {
    
    
    
    // MARK: - Properties
    var cLLocation: CLLocation
    var coordinate: String {
        return String(format: "%.6f,%.6f", cLLocation.coordinate.latitude, cLLocation.coordinate.longitude)
    }
    
    var name: String?
    var googlePlaceId: String?
    
    
    var currentWeather: CurrentWeather?
    var todayForecast: DailyWeather?
    var hourlyForecast: [HourlyWeather]?        // for the next 48 hs
    var weekForecast: [DailyWeather]?           // tomorrow up to 7 days

    
    weak var delegate: UserLocationDelegate?
    
    static var selected: UserLocation?
    static var allLocations: [UserLocation] = []
    
   
    // MARK: - Types for enconding
    struct PropertyKey {
        static let name = "name"
        static let googlePlaceId = "googlePlaceId"
        static let cLLocation = "cLLocation"
    }
    
    //MARK: NSCoding
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: PropertyKey.name)
        aCoder.encode(googlePlaceId, forKey: PropertyKey.googlePlaceId)
        aCoder.encode(cLLocation, forKey: PropertyKey.cLLocation)

    }
    
    //MARK: Archiving Paths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveUrl = DocumentsDirectory.appendingPathComponent("userLocations")
    
    
    
    
    
    
    
    //MARK: Initialization
    required convenience init?(coder aDecoder: NSCoder) {
        guard let clLocation = aDecoder.decodeObject(forKey: PropertyKey.cLLocation) as? CLLocation
            else {
                os_log("Unable to decode the clLocation for a UserLocation object.", log: OSLog.default, type: .debug)
                return nil
        }
        let name = aDecoder.decodeObject(forKey: PropertyKey.name) as? String
        let googlePlaceId = aDecoder.decodeObject(forKey: PropertyKey.googlePlaceId) as? String
        
        self.init(with: clLocation, googleName: name, placeId: googlePlaceId)
    }
    
    
    init(with location: CLLocation) {
        self.cLLocation = location
    }
    
    
    convenience init(with location: CLLocation,  googleName: String?, placeId: String?) {
        self.init(with: location)
        name = googleName
        googlePlaceId = placeId
    }
    
    
    
    
    
    // MARK: - Methods
    static func saveLocations() {
        let locations = UserLocation.allLocations
            let success = NSKeyedArchiver.archiveRootObject(locations, toFile: UserLocation.ArchiveUrl.path)
            
            if success {
                os_log("Locations successfully saved.", log: OSLog.default, type: .debug)
            } else {
                os_log("Failed to save locations...", log: OSLog.default, type: .error)
            }
    }
    
    
    static func loadLocations() -> [UserLocation]? {
        if let locations = NSKeyedUnarchiver.unarchiveObject(withFile: UserLocation.ArchiveUrl.path) as? [UserLocation] {
            UserLocation.allLocations = locations
            return UserLocation.allLocations
        }
        return nil
    }
    
    
    
    static func existingLocation(location: CLLocation) -> UserLocation? {
        if let existingLocation = UserLocation.allLocations
            .filter({
                let coord2D = $0.cLLocation.coordinate
                let stringCoord = String(format: "%.1f,%.1f", coord2D.latitude, coord2D.longitude)  // reduce precision to compare cities
                let newCoordinate = String(format: "%.1f,%.1f", location.coordinate.latitude, location.coordinate.longitude)
                return stringCoord == newCoordinate
            }).first {
            return existingLocation
        } else {
            return nil
        }
    }
    
    
    
    
    func becomeSelectedLocation() {
        UserLocation.selected = self
    }
    
    
    
    static let BasePath = "https://api.darksky.net/forecast/b3700942a7d8c96a424fe85bafbfc283/"
    
    func fetchWeather(fetchType: FetchType, completionHandler:  (() -> Void)? ) {
        guard let url = URL(string: UserLocation.BasePath + coordinate + fetchType.rawValue)
            else {
                print("Invalid URL.")
                return
        }
        let request = URLRequest(url: url)
        let session = URLSession.shared
        
        session.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            guard error == nil
                else {
                    print("\n\n    Session error: \(error.debugDescription) \n\n     Response: \(String(describing: (response as? HTTPURLResponse)?.statusCode))\n\n")
                    return
            }
            
            // parsing
            if let data = data {
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : Any] {
                        if let currently = json["currently"] as? [String : Any] {
                            self.currentWeather = CurrentWeather(json: currently, coordinate: self.coordinate)
                        }
                        
                        if let daily = json["daily"] as? [String : Any], let data = daily["data"] as? [[String : Any]] {
                            self.weekForecast = []
                            data.forEach{ (day) in
                                if let daily = DailyWeather(json: day, coordinate: self.coordinate) {
                                    if daily.isToday {
                                        self.todayForecast = daily
                                    } else {
                                        self.weekForecast!.append(daily)
                                    }
                                }
                            }
                        }
                        
                        if let hourly = json["hourly"] as? [String : Any], let data = hourly["data"] as? [[String : Any]] {
                            data.forEach{ (hourData) in
                                if let hour = HourlyWeather(json: hourData, coordinate: self.coordinate) {
                                    if Calendar.current.isDateInToday(hour.utcTime) {
                                        if self.todayForecast?.hourly == nil {
                                            self.todayForecast?.hourly = []
                                        }
                                        self.todayForecast?.hourly?.append(hour)
                                        
                                    } else  {
                                        if self.weekForecast?.first?.hourly == nil {
                                            self.weekForecast?.first?.hourly = []
                                        }
                                        
                                        if Calendar.current.isDateInTomorrow(hour.utcTime) {
                                            self.weekForecast?.first?.hourly?.append(hour)
                                        } else {
                                            // date is after tomorrow (weekForecast[1])
                                            if let week = self.weekForecast, week.count > 0 {
                                                if week[1].hourly == nil {
                                                    week[1].hourly = []
                                                }
                                                week[1].hourly?.append(hour)
                                            }
                                        }
                                    }
                                    self.hourlyForecast?.append(hour)
                                }
                            }
                        }
                    }
                } catch {
                    print("Error parsing Json: ",error.localizedDescription)
                }
                
                if completionHandler != nil {
                    completionHandler!()
                }
            }
            }.resume()
    }
    
    
    
    
    

    
    // get place name for coordinate/cllocation  ( city, administrativeArea, country )
    func reverseGeoCode(){
        
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(self.cLLocation, completionHandler: { (placemarks, error) in
            if let placemark = placemarks?.first {
                if let placeAddress = placemark.addressDictionary as? [String : Any] {

                    let validFields = ["Country", "CountryCode", "State", "City"]
                    let validAddress = placeAddress.filter{ validFields.contains($0.key)} as! [String:String]

                    var name: String?
                    if let city = validAddress["City"], let state = validAddress["State"] {
                        name = ("\(city), \(state)")
                    } else if let city = validAddress["City"], let country = validAddress["CountryCode"] ?? validAddress["Country"] {
                        name = ("\(city), \(country)")
                    }
                    
                    self.name = name
                    self.delegate?.didSetSelectedLocationName(name: name!)
                }
            } else {
                print(String(describing: error))
            }
        })
       
        
        
       
    }
    

    
    
    
    
    
    
}
