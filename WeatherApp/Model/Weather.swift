////
////  Weather.swift
////  WeatherApp
////
////  Created by Silvia Florido on 23/01/18.
////  Copyright © 2018 Silvia Florido. All rights reserved.
////
//
//import Foundation
//import CoreLocation
//
//struct Weather {
//    
//    // json
//    let currentTemperature: Double
//    var windSpeed: Double?
//    let shortSummary: String
//    var icon: String?
//    let utcDate: Date
//    
//    
//    // local input
//    var location: String   // coordinates
//    var placemark: CLPlacemark? 
//    
//    
//    
//    init?(json: [String : Any], location: String) {
//        guard let temperature = json["temperature"] as? Double,
//            let shortSummary = json["summary"] as? String,
//            let date = json["time"] as? Double
//        else {
//                print("No temperature or shortSummary")
//                return nil
//        }
//    
//        self.utcDate = Date(timeIntervalSince1970: date)
//        self.location = location
//        self.currentTemperature = temperature
//        self.shortSummary = shortSummary
//        
//        self.windSpeed = json["windSpeed"] as? Double
//        self.icon = json["icon"] as? String // case nil just ignore
//    }
//    
//    
//    
//    
//    
//    static let basePath = "https://api.darksky.net/forecast/b3700942a7d8c96a424fe85bafbfc283/"
//    static let query = "?units=si&exclude=minutely,hourly"
//    
//    static func fetchCurrentWeather(coordinates: String, completionHandler: @escaping (Weather?, [DailyWeather]) -> Void) {
//        guard let url = URL(string: basePath + coordinates + query) else { print("Invalid URL."); return }
//        
//        let request = URLRequest(url: url)
//        let session = URLSession.shared
//        
//        session.dataTask(with: request) { (data: Data?, response: URLResponse?, err: Error?) in
////             print((response as? HTTPURLResponse) ?? "No Response" )    // Optional
//            
//            guard let data = data else {   print(err ?? "No data error.");  return  }
//            
//            var currentWeather: Weather?
//            var dailyWeather: [DailyWeather] = []
//            
//            do {
//                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : Any] {
//
//                    // currently - no daily info like maxTemp, minTemp
//                    if let currently = json["currently"] as? [String : Any] {
//                        currentWeather = Weather(json: currently, location: coordinates)
//                    }
//                    
//                    // daily info - more info about the day
//                    if let daily = json["daily"] as? [String : Any] {
//                        if let data = daily["data"] as? [[String : Any]] {
//                            data.forEach{ (day) in
//                                if let daily = DailyWeather(json: day) {
//                                    dailyWeather.append(daily)
//                                }
//                            }
//                        }
//                    }
//                
//                }
//            } catch {
//                print("Error parsing Json: ",error.localizedDescription)
//            }
//            
//            completionHandler(currentWeather, dailyWeather)
//            }.resume()
//    }
//    
//    
//}
//
//struct DailyWeather {
//    
//    let temperatureHigh: Double?
//    let temperatureLow: Double?
//    let sunriseTime: Date?
//    let sunsetTime: Date?
//    let utcDate: Date
//    let icon: String?
//    let precipProbability: Double?
//    let feelsLike: String?
//    
//    
//    
//    
//    init?(json: [String : Any]) {
//
//        guard let date = json["time"] as? Double else {  return nil }
//        self.utcDate = Date(timeIntervalSince1970: date)    // required
//        
//        self.temperatureHigh = json["temperatureHigh"] as? Double
//        self.temperatureLow = json["temperatureLow"] as? Double
//        self.sunriseTime = json["sunriseTime"] as? Date
//        self.sunsetTime = json["sunsetTime"] as? Date
//        self.icon = json["icon"] as? String
//        self.precipProbability = json["precipProbability"] as? Double
//        self.feelsLike = json["apparentTemperature"] as? String
//    }
//    
//    
//
//    
//    
//    
//    
//}
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
