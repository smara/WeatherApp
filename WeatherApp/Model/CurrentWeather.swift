//  Weather.swift
//  WeatherApp
//
//  Created by Silvia Florido on 23/01/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import Foundation
import CoreLocation

enum SerializationError: Error {
    case missing(String)
    case invalid(String, Any)
}

class CurrentWeather {
    
    // json required
    let currentTemperature: Double
    let shortSummary: String
    let utcDate: Date
    
    // json optional
    let windSpeed: Double?
    let windDegree: Double?  // wind direction
    var windDirection: String? {
        if let wind = windDegree {
            switch wind {
            case 337.5...360, 0...22.5: return "N"
            case 22.6...67.5 : return "NE"
            case 67.6...112.5 : return "L"
            case 112.6...157.5 : return "SE"
            case 157.6...202.5 : return "S"
            case 202.6...247.5 : return "SO"
            case 247.6...292.5 : return "O"
            case 292.6...337.5 : return "NO"
            default : return ""
            }
        }
        return nil
    }
    
    let icon: String?
    let feelsLike: Double?
    
  
    var placemark: CLPlacemark?
    let coordinate: String
    
    
    init?(json: [String : Any], coordinate: String) {
        guard let temperature = json["temperature"] as? Double,
            let shortSummary = json["summary"] as? String,
            let date = json["time"] as? Double
            
            else {
                print(SerializationError.missing("Not enough data for Current Weather"))
                return nil
        }
        
        self.coordinate = coordinate
        self.currentTemperature = temperature
        self.shortSummary = shortSummary
        self.utcDate = Date(timeIntervalSince1970: date)

        self.windSpeed = json["windSpeed"] as? Double
        self.windDegree = json["windBearing"] as? Double
        self.icon = json["icon"] as? String
        self.feelsLike = json["apparentTemperature"] as? Double
    }
    
    
    
}






class DailyWeather {
    
    // json
    let utcDate: Date    // UTC - TimeZone 0, needs context using a calendar
    let temperatureHigh: Double?
    let temperatureLow: Double?
    let icon: String?
    let precipProbability: Double?
    
    var isToday: Bool {
        let utcDateComp = Calendar.current.dateComponents([.day, .month, .year], from: utcDate)
        let today = Calendar.current.dateComponents([.day, .month, .year], from: Date())
        return utcDateComp == today
    }
    
    var hourly: [HourlyWeather]?
    
    let sunriseTime: String?
    let sunsetTime: String?
    let summary: String?
    
    let coordinate: String
    
    
    
    init?(json: [String : Any], coordinate: String) {
        guard let date = json["time"] as? Double
            else {
            print(SerializationError.missing("Not enough data for Daily Weather"))
            return nil
        }
        
        utcDate = Date(timeIntervalSince1970: date)    // required

        if let sunrise = json["sunriseTime"] as? Double, let sunset = json["sunsetTime"] as? Double {
            let sunriseDate = Date(timeIntervalSince1970: sunrise)
            let sunsetDate = Date(timeIntervalSince1970: sunset)
            let formatter = DateFormatter()
            formatter.dateFormat = "h:mm a"
            sunriseTime = (formatter.string(from: sunriseDate))
            sunsetTime = (formatter.string(from: sunsetDate))
        } else {
            sunriseTime = nil
            sunsetTime = nil
        }
        
        temperatureHigh = (json["temperatureHigh"] as? Double)?.rounded()
        temperatureLow = (json["temperatureLow"] as? Double)?.rounded()
        icon = json["icon"] as? String
        
        if let precipitation = json["precipProbability"] as? Double {
            precipProbability = precipitation * 100
        } else {
            precipProbability = nil
        }
        
        summary = json["summary"] as? String
        self.coordinate = coordinate
    }

    
    
}



class HourlyWeather {
    
    
    var icon: String?
    let utcTime: Date
    var temperature: Double?
    let coordinate: String
    
    
    
    
    init?(json: [String : Any], coordinate: String) {
       // required
        guard let time = json["time"] as? Double else { return nil }
        utcTime =  Date(timeIntervalSince1970: time)
        self.coordinate = coordinate
        
        // optional
        icon = json["icon"] as? String
        temperature = json["temperature"] as? Double
    }
    
    
    
    
    
    
    
}
































