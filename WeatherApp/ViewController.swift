//
//  ViewController.swift
//  WeatherApp
//
//  Created by Silvia Florido on 23/01/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//
//  Location obtained using Apple's Standard Location Services (GPS) for accuracy (first run or when no location was found). Places details and Autocomplete provided by Google API.

import UIKit
import CoreLocation
import Darwin
import GooglePlaces

class ViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var shortSummary: UILabel!
    @IBOutlet weak var degreeLabel: UILabel!
    @IBOutlet weak var currentTemperature: UILabel!
    @IBOutlet weak var iconImg: UIImageView!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var feelsLikeLabel: UILabel!
    @IBOutlet weak var hiLoLabel: UILabel!
    
    
    @IBOutlet weak var precipitationLabel: UILabel!
    @IBOutlet weak var sunriseLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var sunsetLabel: UILabel!
    
    
   
    
    // MARK: - Model
    var selectedUserLocation: UserLocation?
    var allLocations: [UserLocation] = []
    var manager: CLLocationManager?
    
    
    
    
    
    // MARK: - VC Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        // Location Services (if nil) to get location and create initial model. Should be called only in 1st run ever (after persistence is implemented).
        if selectedUserLocation == nil {
            manager = CLLocationManager()
            manager!.delegate = self
            manager!.requestWhenInUseAuthorization()
            manager!.desiredAccuracy = kCLLocationAccuracyBest
            manager!.distanceFilter = 1000
            manager!.startUpdatingLocation()
            manager!.requestLocation()
        } else {
            fetchWeather(for: selectedUserLocation!)
        }
    }
    
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showLocationsList" {
            if let destination = segue.destination as? LocationsController {
                destination.allLocations = self.allLocations
                destination.currentLocation = self.selectedUserLocation
            }
        }
    }
    
    
    
    private func updateUI() {
        guard let location = selectedUserLocation else {
            // show warning
            // clean UI ?
            return
        }
        
        locationButton.setTitle(location.name ?? location.coordinate, for: .normal)
        
        // current (moment) info
        if let currentWeather = location.currentWeather {
            feelsLikeLabel.text = ""
            shortSummary.text = currentWeather.shortSummary
            currentTemperature.text = String(format: "%.0f",currentWeather.currentTemperature)
            
            if let feelsLike = currentWeather.feelsLike {
                feelsLikeLabel.text = String(format: "Sensação: %.0f °C", feelsLike)
            }
            if let img = currentWeather.icon {
                iconImg.image = UIImage(named: img)
            }
            if let speed = currentWeather.windSpeed, let direction =  currentWeather.windDirection {
                windLabel.text = String(format: "%.0f km/h %@", speed, direction)
            }
        } else {
              print("\n\n Can't update current weather. \n\n")
        }
        
        // daily info
        if let today = location.todayForecast {
            hiLoLabel.text = ""
            precipitationLabel.text = ""
            windLabel.text = ""
            
            if let high = today.temperatureHigh, let low = today.temperatureLow {
                hiLoLabel.text = String(format: "Max: %.0f°   Min: %.0f°", high,low)
            }
            if let precipitation = today.precipProbability {
                precipitationLabel.text = (String(format: "%.0f",precipitation) + " %")
            }
            sunriseLabel.text = today.sunriseTime ?? ""
            sunsetLabel.text = today.sunsetTime ?? ""
        } else {
            print("\n\n Can't update forecast. \n\n")
        }
        
    }
    
    
   

 
    // MARK: - Helpers
    private func fetchWeather(for userLocation: UserLocation) {
        userLocation.fetchWeather(fetchType: .all) { [weak self] in
            DispatchQueue.main.async {
                self?.updateUI()
            }
        }
    }


    func reverseGeoCode(for location: CLLocation){
        // get place name  ( city, administrativeArea, country )
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) in
            if let placemark = placemarks?.first {
                if let placeAddress = placemark.addressDictionary as? [String : Any] {
                    
                    let validFields = ["Country", "CountryCode", "State", "City"]
                    let validAddress = placeAddress.filter{ validFields.contains($0.key)} as! [String:String]
                    
                    var name: String?
                    if let city = validAddress["City"], let state = validAddress["State"] {
                        name = ("\(city), \(state)")
                    } else if let city = validAddress["City"], let country = validAddress["CountryCode"] ?? validAddress["Country"] {
                        name = ("\(city), \(country)")
                    }
                    
                    // got a name, update UI and Model
                    if name != nil, self.selectedUserLocation != nil {
                        self.selectedUserLocation?.name = name
                        self.locationButton.setTitle(name, for: .normal)
                    }
                    
                }
            } else {
                print(String(describing: error))
            }
        })
    }

    
    
   
    
    
    
    // MARK: - CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let newLocation = locations.last {       // Apple: "the most recent location update is at the end of the array"
            
            if selectedUserLocation?.location != nil, selectedUserLocation!.location.distance(from: newLocation) < 1000 {
                return
            }
            
            selectedUserLocation = UserLocation(with: newLocation)
            selectedUserLocation?.location = newLocation
            allLocations.append(selectedUserLocation!)
           
            fetchWeather(for: selectedUserLocation!)
            reverseGeoCode(for: newLocation)
           
            manager.stopUpdatingLocation()
        }
    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
        // ask for more info
    }

}









