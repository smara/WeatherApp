//
//  DemoVC.swift
//  WeatherApp
//
//  Created by Silvia Florido on 08/02/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import Foundation
import CoreLocation


class DemoVC: CurrentWeatherVC {
    
    
    let testLocations = [   "Sao Paulo":["lat":-23.533773,"long":-46.625290],
                            "Rio de Janeiro":["lat":-22.970722,"long":-43.182365],
                            "Chicago":["lat":41.881832,"long":-87.623177]
                        ]
    
    
    
    
    
    
    override func viewDidLoad() {
        _ = UserLocation.loadLocations()
        if UserLocation.allLocations.isEmpty {
            createDemoLocations()
        }
        super.viewDidLoad()
    }
    
    
    
    private func createDemoLocations() {
        testLocations.forEach { (key, value) in
            if let lat = value["lat"], let long = value["long"] {
                let clLocation = CLLocation(latitude: lat, longitude: long)
                let location = UserLocation(with: clLocation)
                location.name = key
                location.fetchWeather(fetchType: .current, completionHandler: nil)
                UserLocation.allLocations.append(location)
            }
        }
        UserLocation.saveLocations()
    }
    
    
    
    
}














