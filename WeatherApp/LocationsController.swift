//
//  LocationsController.swift
//  WeatherApp
//
//  Created by Silvia Florido on 24/01/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import UIKit
import CoreLocation
import GooglePlaces
//import Intents


class LocationsController: UIViewController, UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate, UserLocationDelegate, GMSAutocompleteViewControllerDelegate {
    
    
    @IBOutlet weak var currentTempLabel: UILabel!
    @IBOutlet weak var iconImg: UIImageView!
    @IBOutlet weak var currentLocalLabel: UILabel!
    
    @IBOutlet weak var locationsTable: UITableView!
    @IBOutlet weak var tableHeaderView: LocationsTableHeaderView!
    @IBOutlet weak var currentLocationLabel: UILabel!
    
    let cellId = "locationCell"

//    var allLocationsButSelected: [UserLocation]? {
//        return UserLocation.allLocations.filter{ $0 !== UserLocation.selected! }
//    }
    
    
    var currentDeviceLocation: UserLocation?
    
    lazy var manager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        return manager
    }()
    
    
    
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    
        setupTableHeader()
        manager.requestLocation()   // get current device location
    
        _ = UserLocation.allLocations.map { (location) in
            if location.currentWeather == nil {
                location.delegate = self
                location.fetchWeather(fetchType: .current, completionHandler: nil)
            }
            
        }
    }
    
   
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // dynamic table header height
        guard let headerView = locationsTable.tableHeaderView else {
            return
        }
        let newSize = headerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        if headerView.frame.size.height != newSize.height {
            headerView.frame.size.height = newSize.height
            locationsTable.layoutIfNeeded()
        }
    }
    
    
    
    
   
    
    // shows selected location
    private func setupTableHeader() {
        if let selected = UserLocation.selected {
            tableHeaderView.isLocationSet = true
            if let locationName = selected.name {
                tableHeaderView.placemarkLabel.text = locationName
                if let temp = selected.currentWeather?.currentTemperature {
                    tableHeaderView.currentTemperatureLabel.text = String(format: "%.0f", temp)
                }
            }
            if let today = selected.todayForecast {
                tableHeaderView.summaryLabel.text = today.summary
                if let high = today.temperatureHigh, let low = today.temperatureLow {
                    tableHeaderView.hiLabel.text = String(format: "Max: %.0f°", high)
                    tableHeaderView.loLabel.text = String(format: "Min: %.0f°", low)
                }
            }
        } else {
            tableHeaderView.isLocationSet = false
            
            if UserLocation.allLocations != nil {
                tableHeaderView.noLocationsLabel.isHidden = !UserLocation.allLocations.isEmpty
            }
        }
    }
    
    private func updateCurrentLocationUI() {
        if let current = currentDeviceLocation {
            currentLocalLabel.text = current.name
            if let temperature = current.currentWeather?.currentTemperature {
                currentTempLabel.text = String(format: "%.0f", temperature)
            }
            if let img = current.currentWeather?.icon {
                iconImg.image = UIImage(named: img)
            }
        }
    }
    
    
    
    // MARK: - Actions and helpers
    @IBAction func showDailyForecastButton(_ sender: UIButton) {
        performSegue(withIdentifier: "showDailyForecast", sender: nil)
    }
    
    
    
    // Google Autocomplete List
    @IBAction func searchPlace(_ sender: UIBarButtonItem) {
        let autocompleteVC = GMSAutocompleteViewController()
        autocompleteVC.delegate = self
        let filter = GMSAutocompleteFilter()
        filter.type = GMSPlacesAutocompleteTypeFilter.geocode
        autocompleteVC.autocompleteFilter = filter
        present(autocompleteVC, animated: true, completion: nil)
        
    }
    
    
    // triggered by the google api delegates
    private func createUserLocation(place: GMSPlace) {
        _ = UserLocation.allLocations.map{  print("\n Before Insert: \($0.name)") }

        let location = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        
        // if location already exists, just ignore teh creation
        guard UserLocation.existingLocation(location: location) == nil else {  return  }
        
        let newUserLocation = UserLocation(with: location, googleName: place.name, placeId: place.placeID)
//        newUserLocation.reverseGeoCode() // names comes from Google
//        UserLocation.selected = newUserLocation
        UserLocation.allLocations.append(newUserLocation)
        
        newUserLocation.fetchWeather(fetchType: .current) {
            DispatchQueue.main.async {
                self.locationsTable.reloadData()
                self.setupTableHeader()
            }
        }
//        UserLocation.allLocations = UserLocation.allLocations
        UserLocation.saveLocations()
        _ = UserLocation.allLocations.map{  print("\n After Insert: \($0.name)") }

    }
    
    
    
    
    
    
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return UserLocation.allLocations.count// ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! LocationCell
        let location = UserLocation.allLocations[indexPath.row]
            cell.placemarkLabel.text = location.name
            if let current = location.currentWeather {
                cell.summaryLabel.text = current.shortSummary
                cell.currentTemperatureLabel.text = String(format: "%.0f", current.currentTemperature)
                if let img = current.icon {
                    cell.iconImage.image = UIImage(named: img)
                }
            }
        
        return cell
    }
    
    
    
     func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    
     func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {

        _ = UserLocation.allLocations.map{  print("\n Before Delete: \($0.name)") }
        
//        print("\n Before Delete: \($0.name)")
        if editingStyle == .delete {
            let element = UserLocation.allLocations[indexPath.row]
            print("\n Location being deleted: \(element.name), indexPath: \(indexPath.row)")
            
            UserLocation.allLocations.remove(at: indexPath.row)
            UserLocation.saveLocations()
//            tableView.deleteRows(at: [indexPath], with: .fade)
            tableView.reloadData()
        } else if editingStyle == .insert {
        }
        _ = UserLocation.allLocations.map{  print("\n After Delete: \($0.name)") }
    }
    
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if UserLocation.allLocations != nil, UserLocation.allLocations.count > indexPath.row {

            let locationPressed = UserLocation.allLocations[indexPath.row]
            locationPressed.becomeSelectedLocation()  // automaticamente remove do allButSelected que é computado

            tableView.reloadData()

            setupTableHeader()
        }
    }
    

    
    
    
    // MARK: - GMSAutocompleteViewControllerDelegate Google API   -  triggered by + button
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        createUserLocation(place: place)
        
        dismiss(animated: true, completion: nil)
    }

    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Failed to autocomplete at Locations scene. \n", error)
        dismiss(animated: true, completion: nil)
    }
    
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    

    
    
    
    // MARK: - CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let newCLLocation = locations.last {
           
            if let existingLocation = UserLocation.existingLocation(location: newCLLocation) {
                currentDeviceLocation = existingLocation
                
            } else {
                // cria uma localização temporária
                // só salva se o usuario quiser salvar essa localização (clicar)
                currentDeviceLocation = UserLocation(with: newCLLocation)
                currentDeviceLocation!.reverseGeoCode()
            }
            
            currentDeviceLocation!.delegate = self
            currentDeviceLocation?.fetchWeather(fetchType: .all) { [weak self] in
                DispatchQueue.main.async {
                    self?.updateCurrentLocationUI()
                    
                }
            }
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
        // ask for more info
    }
    
    
    // MARK: - UserLocationDelegate
    func didSetSelectedLocationName(name: String) {
        self.currentLocationLabel.text = name
    }
    
}
















